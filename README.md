I need to block myself out of adult sites using any method possible. This is a collection of ideas and not-finished implementations that you as a developer can easily modify for your needs and set up in your network to make it hard, time consuming&risky for yourself and others on your devices to access some sites.

* Hosts-File or DNS Zones on device and on DNS-Server (router?)
* OpenDNS as upstream DNS (router)
* check specific folders regularly to see weater the TOR-Browser is downloaded, if yes send a mail to a specific mail address
* monitor the hosts-file for changes - send a mail with all changes
* monitor if certain DNS-requests go to specific block-ip-adress
* block TOR directory server IPs on the router
* block specific ports on the router
* redirect all traffic destination on the OpenDNS-Block-IP to an programm that reads out the source (IP reverse lookup) hostname and destination Domain (eighter http-Host-header or SNI) and sends a report mail
* redirect all DNS-Requests to OpenDNS-Server

Challanges:
* Phone
  * locked down
  * lacking skills
* E-Mails containing names of this sites easily get caught or rejected by spamfilters
* not wanting to tempt someone with the hostnames written down here (Lord, protect everyone visiting!)
* not wanting to tempt someone with the urls contained in the warning mails
* all DNS-securing stuff like DoT, DoH, ...
