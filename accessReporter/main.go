package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/go-gomail/gomail"
	"github.com/go-ini/ini"
)

type TLSConfig struct {
	Key  string
	Cert string
}

type MailConfig struct {
	Servername string
	Username   string
	Password   string
	Port       int
	From       string
	To         string
	ToArr      []string
}

type TimingConfig struct {
	Collecting    uint32
	FloodControll uint32
}

type Configuration struct {
	Timing TimingConfig
	Mail   MailConfig
	TLS    TLSConfig
}

type Request struct {
	Hostname  string
	Src       string
	Url       string
	UserAgent string
	Type      int8 // 0: HTTP, 1: HTTPS
}

var (
	conf *Configuration

	requestQ   chan Request
	RqTypeName []string

	//Telling the Mailer when to shut down and reports back when this is finished
	cl *Closer

	//Waiting Packets
	//TODO: used consistently?? avoidable?
	waitingPkgs int32

	//prevent mail floods
	loggingPaused bool
)

func reverseIP(ipPort string) string {
	ip, _, e := net.SplitHostPort(ipPort)
	if e != nil {
		return "[Error]: RemoteAddr not in IP:Port-Syntax"
	}

	reverse, err := net.LookupAddr(ip)
	if err == nil {
		return reverse[0]
	}

	return err.Error()
}

func sendMail(subject string, msg string) (e error) {
	log.Println("Sending Mail ", subject)

	fmt.Println("Subject: " + subject)
	fmt.Print(msg)

	if loggingPaused {
		log.Println("Not sending requested mail because of flood control")
		return
	}

	m := gomail.NewMessage()
	m.SetHeader("From", conf.Mail.From)
	m.SetHeader("To", conf.Mail.ToArr...)
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", msg)

	d := gomail.NewDialer(conf.Mail.Servername, conf.Mail.Port, conf.Mail.Username, conf.Mail.Password)
	if err := d.DialAndSend(m); err != nil {
		log.Println("Error sending mail")
		log.Fatal(err)
		return
	}

	log.Println("sent Mail")
	return
}

func handleShutdown(cl *Closer) {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	//wait for stop
	log.Println("Waiting for shutdown-signal")
	<-sigc
	log.Println("Shutting down")

	//stop
	cl.SignalAndWait()
	log.Println("all packets handled")
	sendMail("Access-Reporter stopped", "The Access-reporter of has just been stopped")
}

func handleRQ(cl *Closer) {
	cl.AddRunning(1)
	defer cl.Done()
run:
	for {
		//arriving request OR cought kill-signal
		select {
		case curReq := <-requestQ: //Request
			log.Println("Handling first request of a series")

			log.Println("Waiting for additional packets (flood controll)")
			//wait for specific time to potentially collect additional Requests (or kill-signal)
			select {
			case <-time.After(time.Duration(conf.Timing.Collecting) * time.Second):
			case <-cl.HasBeenClosed():
				log.Println("breaking sleep")
				//continue to send without the 2 minute wait
			}
			log.Println("Waiting finished - handling packets")

			//craft Message Text including all received Packets
			//pastReq := Request{}
			msg := "Messages:\r\n"
		loopMSGs: //loop through Messages
			for {
				log.Println("handling packet")

				msg += "Type: " + RqTypeName[curReq.Type] + "\r\n"
				msg += "Host: " + curReq.Hostname + "\r\n"
				msg += "From: " + curReq.Src + " (" + reverseIP(curReq.Src) + ")\r\n"
				if curReq.Url != "" {
					msg += "Url: " + curReq.Url + "\r\n"
				}
				if curReq.UserAgent != "" {
					msg += "Browser: " + curReq.UserAgent + "\r\n"
				}

				msg += "\r\n"

				//pastReq = curReq
				//additional Messages available? Otherwise break
				select {
				case curReq = <-requestQ:
				default:
					if atomic.LoadInt32(&waitingPkgs) <= 0 { //messages may need to come out of the queue
						break loopMSGs
					}
				}
			}
			log.Println("all packets handled")

			//send :)
			sendMail("Access Report", msg)

			log.Printf("wait for %d Secounds to prevent flood", conf.Timing.FloodControll)
			go func() {
				loggingPaused = true
				select {
				case <-time.After(time.Duration(conf.Timing.FloodControll) * time.Second):
					loggingPaused = false
				}
				log.Println("The wait is over - a new packet is handled like first packet again")
			}()
		case <-cl.HasBeenClosed():
			break run
		}
	}

	log.Println("stopping packet handler")
}

func main() {
	conf = new(Configuration)

	//load config
	var confpath string
	/*	if length(os.Args) > 1 {
		confpath = os.Args[1]
	} else {*/
	confpath = "/usr/local/share/accessReporter/config.ini"
	//	}

	if err := ini.MapTo(conf, confpath); err != nil {
		log.Fatal(err)
		panic("Configuration couldn't be loaded!")
	}

	conf.Mail.ToArr = strings.Split(conf.Mail.To, ",")

	if _, err := os.Stat(conf.TLS.Key); os.IsNotExist(err) {
		err := exec.Command("openssl", "req", "-nodes", "-x509", "-newkey", "rsa:4096", "-subj", "/O=accessReporter/CN=anydomain", "-keyout", conf.TLS.Key, "-out", conf.TLS.Cert, "-days", "3650").Run()
		if err != nil {
			log.Fatal(err)
			panic("Keys couldn't be generated!")
		}
	}

	//init
	loggingPaused = false
	cl = NewCloser(0)
	RqTypeName = []string{"unencrypted (or decrypted) web request", "encrypted web request"}
	requestQ = make(chan Request)

	//HTTP
	go func() {
		srv := &http.Server{
			Addr:    ":80",
			Handler: &handler{},
		}

		log.Println("starting HTTP Server")
		log.Fatal(srv.ListenAndServe())
	}()

	//HTTPS
	go func() {
		srv := &http.Server{
			Addr:      ":443",
			Handler:   &handler{},
			TLSConfig: &tls.Config{GetConfigForClient: onHallo},
		}

		log.Println("starting HTTPS Server")
		log.Fatal(srv.ListenAndServeTLS(conf.TLS.Cert, conf.TLS.Key))
	}()

	go handleRQ(cl)
	log.Println("send start-Message")
	sendMail("Access-Reporter started", "The Access-reporter of has just been started")
	handleShutdown(cl)
}

func asyncServerWrite(dst chan Request, src Request) {
	if loggingPaused == false {
		go func() {
			log.Println("new pkg to handle")
			defer log.Println("pkg handled")

			atomic.AddInt32(&waitingPkgs, 1)
			defer atomic.AddInt32(&waitingPkgs, -1)

			dst <- src
		}()
	}
}

/**
 * HTTPS
 */
func onHallo(pkg *tls.ClientHelloInfo) (cfg *tls.Config, err error) {
	asyncServerWrite(requestQ, Request{
		Hostname: pkg.ServerName,
		Src:      pkg.Conn.RemoteAddr().String(),
		Type:     1,
	})

	return
}

/**
 * HTTP
 */
type handler struct{}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	asyncServerWrite(requestQ, Request{
		Hostname:  r.Host,
		Src:       r.RemoteAddr,
		Type:      0,
		Url:       r.URL.String(),
		UserAgent: r.UserAgent(),
	})

	httpResponse(w, r)
}

func httpResponse(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "Sorry, but this content can't be accessed ;(")
}
