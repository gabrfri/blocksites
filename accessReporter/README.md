Sadly currently networking in docker is horrible
* assigning ips accessible from the outside
* deploying a container with multiple networks - setting the default interface
  * Bugs associated with this
    * https://github.com/moby/moby/issues/20179
    * https://github.com/docker/compose/issues/6822

Currently you need to do a pretty specific pre-setup to get it working ...

If someone wants to help make it better for everyone, PLEASE DO SO!

Pre-setup:
==========
TODO: Replace with a dummy-Interface and routing inside this interface (and a static Route on the router).

On your router set up a VLAN (id 146 in my case) for the accessReporter.
* IP-Range 146.112.61.104/29
* Disable DHCP on this Interface
* Make it reachable from LAN - in a multi-router-setup static routes
* Make it reach the WAN BUT through a NAT/PAT (for sending Mails)

Setup 8021q on the server on this interface. (eg. through the not finished ansible-script) It theoretically doesn't need a IP, still a IP is maybe helpfull for debugging purposes (but .106 needs to be the container!!).

Set up a Mailbox

Setup:
======
(Maybe I am missing something)
Modify `docker-compose.yml` and `cnf/config.ini`.
```
docker build -t accessreporter:latest .
docker-compose up -d
```
